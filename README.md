# Coturn

-> Workaround for Ubuntu 22.04 (see https://paul.totterman.name/posts/coturn-ubuntu-jammy/).

## Add repo signing key to apt

```bash
sudo curl -sL -o /etc/apt/trusted.gpg.d/morph027-coturn.asc https://packaging.gitlab.io/coturn/gpg.key
```

## Add repo to apt

```bash
. /etc/lsb-release; echo "deb https://packaging.gitlab.io/coturn/$DISTRIB_CODENAME $DISTRIB_CODENAME main" | sudo tee /etc/apt/sources.list.d/morph027-coturn.list
```

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).
