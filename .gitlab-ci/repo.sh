#!/bin/bash

set -e

gpg --import <<<"${SIGNING_KEY}"
test -f "${CI_PROJECT_DIR}/.repo/gpg.key" || gpg --export --armor "${SIGNING_KEY_ID}" > "${CI_PROJECT_DIR}/.repo/gpg.key"
for dist in jammy
do
  sed -i 's,##SIGNING_KEY_ID##,'"${SIGNING_KEY_ID}"',' "${CI_PROJECT_DIR}/.repo/${dist}/conf/distributions"
  mkdir -p "${CI_PROJECT_DIR}/build-${dist}-dummy-dir-for-find-to-succeed"
  
  if [[ "${DEBUG}" ]]; then
    set -x
  fi
  
  mapfile -t packages < <(find "${CI_PROJECT_DIR}/build-${dist}-"* -type f -name "*.deb")
  
  includedebs=()
  
  for package in "${packages[@]}"
  do
    package_name="$(dpkg -f "${package}" Package)"
    package_version="$(dpkg -f "${package}" Version)"
    package_arch="$(dpkg -f "${package}" Architecture)"
    printf "\e[1;36mChecking for package %s %s (%s) in current repo cache ...\e[0m " "${package_name}" "${package_version}" "${package_arch}"
    case "${package_arch}" in
      "all")
        filter='Package (=='"${package_name}"'), $Version (=='"${package_version}"')'
        ;;
      *)
        filter='Package (=='"${package_name}"'), $Version (=='"${package_version}"'), $Architecture (=='"${package_arch}"')'
        ;;
    esac
    if [ -d "${CI_PROJECT_DIR}/.repo/${dist}/db" ]; then
      if reprepro -b "${CI_PROJECT_DIR}/.repo/${dist}" listfilter "${dist}" "${filter}" | grep -q '.*'; then
        printf "\e[0;32mOK\e[0m\n"
        continue
      fi
    fi
    if grep -q "${package##*/}" <<<"${includedebs[@]}"; then
      printf "\e[0;32mOK\e[0m\n"
      continue
    fi
    printf "\e\033[0;38;5;166mAdding\e[0m\n"
    includedebs+=("${package}")
  done
  
  if [ -n "${includedebs}" ]; then
    reprepro \
      -v \
      -b "${CI_PROJECT_DIR}/.repo/${dist}" \
      includedeb \
      "${dist}" \
      "${includedebs[@]}"
  fi
  
  if ! reprepro -v -b "${CI_PROJECT_DIR}/.repo/${dist}" checkpool fast |& tee /tmp/missing; then
    printf "\e[0;36mStarting repo cache cleanup ...\e[0m\n"
    mapfile -t missingfiles < <(grep "Missing file" /tmp/log | grep --color=never -o "/.*\.deb")
    for missingfile in "${missingfiles[@]}"
    do
      missingfile="${missingfile##*/}"
      name="$(cut -d'_' -f 1 <<<"${missingfile}")"
      version="$(cut -d'_' -f 2 <<<"${missingfile}")"
      echo "  cleanup missing file ${missingfile} from repo"
      reprepro \
        -v \
        -b "${CI_PROJECT_DIR}/.repo/${dist}" \
        remove \
        "${dist}" \
        "${name}=${version}"
    done
  fi
done
