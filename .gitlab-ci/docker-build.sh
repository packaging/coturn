#!/bin/ash

set -e

if [ "${DEBUG}" ]; then
  set -x
fi

cleanup_error() {
  docker rm -f "coturn-builder-${CI_JOB_ID}"
  exit 1
}

docker rm -f "coturn-builder-${CI_JOB_ID}" || true
DOCKER_DEFAULT_PLATFORM="linux/${ARCH}" docker create --name "coturn-builder-${CI_JOB_ID}" \
  -e CI_PROJECT_DIR=/build \
  -e CI_JOB_NAME="${CI_JOB_NAME}" \
  -e ARCH="${ARCH}" \
  -e DEBUG="${DEBUG}" \
  ubuntu:"${DIST}" /build/.gitlab-ci/build.sh
docker cp "${CI_PROJECT_DIR}" "coturn-builder-${CI_JOB_ID}":/build || cleanup_error
docker start "coturn-builder-${CI_JOB_ID}" || cleanup_error
docker attach "coturn-builder-${CI_JOB_ID}" || cleanup_error
docker cp "coturn-builder-${CI_JOB_ID}":/build /tmp/
docker rm -f "coturn-builder-${CI_JOB_ID}"
cp -rv /tmp/build/"${CI_JOB_NAME}" "${CI_PROJECT_DIR}"/
