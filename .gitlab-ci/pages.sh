#!/bin/bash

set -e

for dist in jammy
do
  mkdir -p "${CI_PROJECT_DIR}"/public/"${dist}"
  cp -rv \
    "${CI_PROJECT_DIR}"/.repo/"${dist}"/{pool,dists} \
    "${CI_PROJECT_DIR}"/public/"${dist}"/
done
cp -rv \
  "${CI_PROJECT_DIR}"/.repo/gpg.key \
  "${CI_PROJECT_DIR}"/public/
