#!/bin/bash

set -e

export DEBIAN_FRONTEND=noninteractive

. /etc/lsb-release

WORK_DIR="$(mktemp -d)"
chmod 777 "${WORK_DIR}"
cd "${WORK_DIR}"

sed -i 's,^#.*deb-src,deb-src,' /etc/apt/sources.list
grep deb-src /etc/apt/sources.list | sed 's,jammy,impish,' > /etc/apt/sources.list.d/impish.list
apt-get -qq update
apt-get -qqy install dpkg-dev quilt build-essential
apt-get -qqy build-dep coturn
apt-get source coturn
cd coturn-*
quilt push -a || true
quilt new jammy.patch
quilt add src/client/ns_turn_msg.c
patch -p1 < "${CI_PROJECT_DIR}/patches/jammy-openssl"
quilt refresh
quilt pop -a
dpkg-buildpackage -us -uc -j"$(getconf _NPROCESSORS_ONLN)"
eval "$(dpkg-architecture)"
mkdir -p "$CI_PROJECT_DIR/build-${DISTRIB_CODENAME}-${DEB_BUILD_ARCH}"
mv "$WORK_DIR"/*.deb "$CI_PROJECT_DIR/build-${DISTRIB_CODENAME}-${DEB_BUILD_ARCH}/"
